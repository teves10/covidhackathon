import React, {useReducer} from 'react';
import Progress from './components/Progress';
import Question from './components/Question';
import Answers from './components/Answers';
import QuizContext from './context/QuizContext';

import {
    SET_ANSWERS,
    SET_CURRENT_QUESTION,
    SET_CURRENT_ANSWER,
    SET_ERROR,
    SET_SHOW_RESULTS,
    RESET_QUIZ,
} from './reducers/types.js';
import quizReducer from './reducers/QuizReducer';

import './App.css';

function App() {
    const questions = [
        {
            id: 1,
            question: 'What are the most common signs and symptoms of COVID-19?',
            answer_a:
                'Cold, dry skin',
            answer_b: 'Fever, vomiting and constant thirst',
            answer_c: 'Earache and sore joints',
            answer_d: 'Fever, fatigue and dry cough',
            correct_answer: 'd',
        },
        {
            id: 2,
            question: 'What does social distancing mean?',
            answer_a: 'Keeping at least 1.5 metres away from others',
            answer_b: 'Not leaving your house',
            answer_c: 'Stay at leave five metres away from others',
            answer_d: 'It does not mean anything as it is not important',
            correct_answer: 'a',
        },
        {
            id: 3,
            question: 'How does transmission of COVID-19 occur?',
            answer_a: 'Contact with blood',
            answer_b: 'Droplet transmission',
            answer_c: 'Through the skin',
            answer_d: 'Drinking contaminated water',
            correct_answer: 'b',
        },
        {
            id: 4,
            question: 'What precautions do yu need to take while handling and preparing food?',
            answer_a: 'Have separate choppiong boards for your cooked and uncooked meat',
            answer_b: 'Practice hand hygiene',
            answer_c: 'ensure all meats are cooked thoroughly',
            answer_d: 'All of the above',
            correct_answer: 'd',
        },
        {
            id: 5,
            question: 'COVID-19 is a ',
            answer_a: 'Bacteria',
            answer_b: 'Fungus',
            answer_c: 'Virus',
            answer_d: 'Parasite',
            correct_answer: 'c',
        },
        {
            id: 6,
            question: 'How can you protect yourself from COVID-19?',
            answer_a: 'Washing hands regulartly or use alcohol-based hand sanitizer rub',
            answer_b: 'Avoid touching your eyes, nose and mouth',
            answer_c: 'Cover your cough or sneeze with a tissue and dispose of it immediately',
            answer_d: 'All of the above',
            correct_answer: 'd',
        },
        {
            id: 7,
            question: 'Which demographic groups are most likely to become seriously ill infected?',
            answer_a: 'Elderly',
            answer_b: 'Middle Age',
            answer_c: 'Women',
            answer_d: 'Children',
            correct_answer: 'a',
        },
        {
            id: 8,
            question: 'Who is responsible for preventing the spread of COVID-19?',
            answer_a: 'Doctors',
            answer_b: 'Everyone',
            answer_c: 'Community',
            answer_d: 'Nurses',
            correct_answer: 'b',
        },
    ];

    const initialState = {
        questions,
        currentQuestion: 0,
        currentAnswer: '',
        answers: [],
        showResults: false,
        error: '',
    };

    const [state, dispatch] = useReducer(quizReducer, initialState);
    const {currentQuestion, currentAnswer, answers, showResults, error} = state;

    const question = questions[currentQuestion];

    const renderError = () => {
        if (!error) {
            return;
        }

        return <div className="error" style={{color:"red"}}>{error}</div>;
    };

    const renderResultMark = (question, answer) => {
        if (question.correct_answer === answer.answer) {
            return <span className="correct">Correct</span>;
        }

        return <span className="failed" style={{color:"red"}}>Failed</span>;
    };

    const renderResultsData = () => {
        return answers.map(answer => {
            const question = questions.find(
                question => question.id === answer.questionId
            );

            return (
                <div key={question.id}>
                    {question.question} - {renderResultMark(question, answer)}
                </div>
            );
        });
    };

    const restart = () => {
        dispatch({type: RESET_QUIZ});
    };

    const next = () => {
        const answer = {questionId: question.id, answer: currentAnswer};

        if (!currentAnswer) {
            dispatch({type: SET_ERROR, error: 'Please select an option'});
            return;
        }

        answers.push(answer);
        dispatch({type: SET_ANSWERS, answers});
        dispatch({type: SET_CURRENT_ANSWER, currentAnswer: ''});

        if (currentQuestion + 1 < questions.length) {
            dispatch({
                type: SET_CURRENT_QUESTION,
                currentQuestion: currentQuestion + 1,
            });
            return;
        }

        dispatch({type: SET_SHOW_RESULTS, showResults: true});
    };

    if (showResults) {
        return (
            <div className="container results" style={{backgroundColor: "#84a9ac"}}>
                <h2 style={{color: "#204051"}}>Results</h2>
                <ul style={{color: "#204051"}}>{renderResultsData()}</ul>
                <button className="btn " onClick={restart}
                style={{
                    backgroundColor: "#204051",
                    color:"white",    
                    borderRadius: "50px"
                }}
                >
                    Restart
                </button>
            </div>
        );
    } else {
        return (
            <QuizContext.Provider value={{state, dispatch}}>
                <div className="container"
                style={{
                    backgroundColor: "#84a9ac"
                    
                }}>
                    <Progress
                        total={questions.length}
                        current={currentQuestion + 1}
                    />
                    <Question />
                    {renderError()}
                    <Answers w/>
                    <button className="btn" onClick={next}
                    style={{
                        backgroundColor: "#204051",
                        color: "white",
                        borderRadius: "50px"
                        
                    }}
                    >
                        Confirm and Continue
                    </button>
                </div>
            </QuizContext.Provider>
        );
    }
}

export default App;
