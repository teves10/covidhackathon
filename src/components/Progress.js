import React from 'react';

function Progress(props) {
    return (
        <h2 style={{color:"#3b6978"}}>
            Question {props.current} of {props.total}
        </h2>
    );
}

export default Progress;
